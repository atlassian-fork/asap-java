package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import org.apache.http.client.HttpClient;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.PublicKey;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class PublicKeyProviderFactoryTest {
    @Mock
    private PemReader pemReader;
    @Mock
    private HttpClient httpClient;

    @InjectMocks
    private PublicKeyProviderFactory factory;

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectUnknownSchemes() {
        factory.createPublicKeyProvider("unknown://baseurl");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldRejectInsecureHttp() {
        factory.createPublicKeyProvider("http://example.test/");
    }

    @Test
    public void shouldCreateHttpsProvider() {
        KeyProvider<PublicKey> provider = factory.createPublicKeyProvider("https://example.test/");
        assertThat(provider, instanceOf(HttpPublicKeyProvider.class));
    }

    @Test
    public void shouldCreateFileProvider() {
        KeyProvider<PublicKey> provider = factory.createPublicKeyProvider("file:///some/location/");
        assertThat(provider, instanceOf(FilePublicKeyProvider.class));
    }

    @Test
    public void shouldCreateClasspathProvider() {
        KeyProvider<PublicKey> provider = factory.createPublicKeyProvider("classpath:/package/");
        assertThat(provider, instanceOf(ClasspathPublicKeyProvider.class));
        assertThat(((ClasspathPublicKeyProvider) provider).getClasspathBase(), is("/package/"));
    }

    @Test
    public void shouldCreateMirroredKeyProvider() {
        KeyProvider<PublicKey> provider = factory.createPublicKeyProvider("file:///some | https://test/");
        assertThat(provider, instanceOf(MirroredKeyProvider.class));
        assertThat(((MirroredKeyProvider) provider).getMirrors(), hasSize(2));
    }

    @Test
    public void shouldCreateChainedKeyProvider() {
        KeyProvider<PublicKey> provider = factory.createPublicKeyProvider("file:///some , https://test/");
        assertThat(provider, instanceOf(ChainedKeyProvider.class));
        assertThat(((ChainedKeyProvider) provider).getKeyProviderChain(), hasSize(2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenBaseUrlDoesNotContainLeadingWhitespaceForChainedProviders() {
        factory.createPublicKeyProvider("file:///some, https://test/");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenBaseUrlDoesNotContainTrailingWhitespaceForChainedProviders() {
        factory.createPublicKeyProvider("file:///some ,https://test/");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenBaseUrlDoesNotContainLeadingAndTrailingWhitespaceForChainedProviders() {
        factory.createPublicKeyProvider("file:///some,https://test/");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenBaseUrlDoesNotContainLeadingWhitespaceForMirroredProviders() {
        factory.createPublicKeyProvider("file:///some| https://test/");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenBaseUrlDoesNotContainTrailingWhitespaceForMirroredProviders() {
        factory.createPublicKeyProvider("file:///some |https://test/");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenBaseUrlDoesNotContainLeadingAndTrailingWhitespaceForMirroredProviders() {
        factory.createPublicKeyProvider("file:///some|https://test/");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenSingletonBaseUrlDoesNotHaveSchema() {
        factory.createPublicKeyProvider("/relative/uri");
    }

    @Test(expected = IllegalArgumentException.class)
    public void shouldThrowWhenSingletonBaseUrlCannotBeParsedAsUri() {
        factory.createPublicKeyProvider("123:foo");
    }

    @Test
    public void shouldHandleEmptyBaseUrl() {
        KeyProvider<PublicKey> provider = factory.createPublicKeyProvider("");
        assertThat(provider, instanceOf(ChainedKeyProvider.class));
        assertThat(((ChainedKeyProvider) provider).getKeyProviderChain(), hasSize(0));
    }
}
