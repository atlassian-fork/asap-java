package com.atlassian.asap.nimbus.parser;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.SigningAlgorithm;
import com.atlassian.asap.core.exception.JwtParseException;
import com.atlassian.asap.core.exception.MissingRequiredClaimException;
import com.atlassian.asap.core.exception.MissingRequiredHeaderException;
import com.atlassian.asap.core.exception.UnsupportedAlgorithmException;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableSet;
import com.nimbusds.jose.util.Base64;
import com.nimbusds.jose.util.Base64URL;
import net.minidev.json.JSONObject;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

import javax.annotation.Nullable;
import javax.json.JsonValue;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@RunWith(MockitoJUnitRunner.class)
public class NimbusJwtParserTest {
    private static final String VALID_KID = "my-kid";
    private static final String VALID_TOKEN_ID = "my-token-id";
    private static final String VALID_ISSUER = "my-issuer";
    private static final String VALID_SUBJECT = "my-subject";
    private static final String VALID_AUDIENCE = "my-audience";
    private static final String VALID_SIGNATURE = "some-signature";
    private static final long VALID_NBF = 1L;
    private static final long VALID_IAT = 2L;
    private static final long VALID_EXPIRY = 3L;

    private static final Map<String, Object> VALID_HEADERS = ImmutableMap.<String, Object>of(
            "kid", VALID_KID,
            "alg", "RS256"
    );
    private static final Map<String, Object> VALID_CLAIMS = ImmutableMap.<String, Object>of(
            "jti", VALID_TOKEN_ID,
            "iss", VALID_ISSUER,
            "aud", VALID_AUDIENCE,
            "iat", VALID_IAT,
            "exp", VALID_EXPIRY
    );
    private static final Base64URL SIGNATURE = Base64URL.encode(VALID_SIGNATURE);

    @InjectMocks
    private NimbusJwtParser parser;

    @Test
    public void shouldAcceptValidSignedToken() throws Exception {
        Jwt jwt = parser.parse(serialisedJwt(VALID_HEADERS, VALID_CLAIMS, SIGNATURE));

        assertEquals(SigningAlgorithm.RS256, jwt.getHeader().getAlgorithm());
        assertEquals(VALID_KID, jwt.getHeader().getKeyId());
        assertEquals(VALID_ISSUER, jwt.getClaims().getIssuer());
        assertFalse(jwt.getClaims().getSubject().isPresent());
        assertEquals(ImmutableSet.of(VALID_AUDIENCE), jwt.getClaims().getAudience());
        assertEquals(VALID_IAT, jwt.getClaims().getIssuedAt().getEpochSecond());
        assertEquals(VALID_EXPIRY, jwt.getClaims().getExpiry().getEpochSecond());
        assertFalse(jwt.getClaims().getNotBefore().isPresent());
    }

    @Test
    public void shouldParseStringPrivateClaimIfAvailable() throws Exception {
        Map<String, Object> claims = add(VALID_CLAIMS, "privateClaim", "privateClaimValue");
        Jwt jwt = parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));

        assertEquals("privateClaimValue", jwt.getClaims().getJson().getString("privateClaim"));
    }

    @Test
    public void shouldParseNullPrivateClaimIfAvailable() throws Exception {
        Map<String, Object> claims = add(VALID_CLAIMS, "privateClaim", null);
        Jwt jwt = parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));

        assertEquals(JsonValue.NULL, jwt.getClaims().getJson().get("privateClaim"));
    }

    @Test
    public void shouldParseSubjectIfAvailable() throws Exception {
        Map<String, Object> claims = add(VALID_CLAIMS, "sub", VALID_SUBJECT);
        Jwt jwt = parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));

        assertEquals(VALID_SUBJECT, jwt.getClaims().getSubject().get());
    }

    @Test
    public void shouldParseNotBeforeDateIfAvailable() throws Exception {
        Map<String, Object> claims = add(VALID_CLAIMS, "nbf", VALID_NBF);
        Jwt jwt = parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));

        assertEquals(VALID_NBF, jwt.getClaims().getNotBefore().get().getEpochSecond());
    }

    @Test
    public void shouldParseMultipleAudiences() throws Exception {
        Set<String> audiences = ImmutableSet.of(VALID_AUDIENCE, "another-audience");
        Map<String, Object> claims = add(remove(VALID_CLAIMS, "aud"), "aud", audiences);

        Jwt jwt = parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));

        assertEquals(audiences, jwt.getClaims().getAudience());
    }

    @Test(expected = MissingRequiredHeaderException.class)
    public void shouldRequireKidHeader() throws Exception {
        Map<String, Object> headers = remove(VALID_HEADERS, "kid");

        parser.parse(serialisedJwt(headers, VALID_CLAIMS, SIGNATURE));
    }

    @Test(expected = JwtParseException.class)
    public void shouldRequireAlgHeader() throws Exception {
        Map<String, Object> headers = remove(VALID_HEADERS, "alg");

        parser.parse(serialisedJwt(headers, VALID_CLAIMS, SIGNATURE));
    }

    @Test(expected = MissingRequiredClaimException.class)
    public void shouldRequireIssClaim() throws Exception {
        Map<String, Object> claims = remove(VALID_CLAIMS, "iss");

        parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));
    }

    @Test(expected = MissingRequiredClaimException.class)
    public void shouldRequireAudClaim() throws Exception {
        Map<String, Object> claims = remove(VALID_CLAIMS, "aud");

        parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));
    }

    @Test(expected = MissingRequiredClaimException.class)
    public void shouldRequireJtiClaim() throws Exception {
        Map<String, Object> claims = remove(VALID_CLAIMS, "jti");

        parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));
    }

    @Test(expected = MissingRequiredClaimException.class)
    public void shouldRequireIatClaim() throws Exception {
        Map<String, Object> claims = remove(VALID_CLAIMS, "iat");

        parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));
    }

    @Test(expected = MissingRequiredClaimException.class)
    public void shouldRequireExpClaim() throws Exception {
        Map<String, Object> claims = remove(VALID_CLAIMS, "exp");

        parser.parse(serialisedJwt(VALID_HEADERS, claims, SIGNATURE));
    }

    @Test(expected = JwtParseException.class)
    public void shouldRejectTokenIfAlgorithmIsNone() throws Exception {
        Map<String, Object> headers = add(remove(VALID_HEADERS, "alg"), "alg", "none");

        parser.parse(serialisedJwt(headers, VALID_CLAIMS, SIGNATURE));
    }

    @Test
    public void shouldDetermineIssuerSucceedIfAvailable() {
        Optional<String> issuer = parser.determineUnverifiedIssuer(serialisedJwt(VALID_HEADERS, VALID_CLAIMS, SIGNATURE));

        assertEquals(VALID_ISSUER, issuer.get());
    }

    @Test
    public void shouldDetermineIssuerReturnEmptyIfIssuerClaimIsMissing() {
        assertFalse(parser.determineUnverifiedIssuer(
                serialisedJwt(VALID_HEADERS, remove(VALID_CLAIMS, "iss"), SIGNATURE)).isPresent());
    }

    @Test
    public void shouldDetermineIssuerReturnEmptyIfJwtDoesNotHaveThreeParts() {
        assertFalse(parser.determineUnverifiedIssuer("not-a-jwt").isPresent());
    }

    @Test
    public void shouldDetermineIssuerReturnEmptyIfJwtHeadersAreUnparseable() {
        String brokenHeaders = Base64.encode("not-a-json-object").toString();
        String validClaims = Base64.encode(JSONObject.toJSONString(VALID_CLAIMS)).toString();
        String irrelevantSignature = SIGNATURE.toString();
        assertFalse(parser.determineUnverifiedIssuer(
                String.join(".", brokenHeaders, validClaims, irrelevantSignature)).isPresent());
    }

    @Test
    public void shouldDetermineIssuerReturnEmptyIfJwtClaimsAreUnparseable() {
        String validHeaders = Base64.encode(JSONObject.toJSONString(VALID_HEADERS)).toString();
        String brokenClaims = Base64.encode("not-a-json-object").toString();
        String irrelevantSignature = SIGNATURE.toString();
        assertFalse(parser.determineUnverifiedIssuer(
                String.join(".", validHeaders, brokenClaims, irrelevantSignature)).isPresent());
    }

    /**
     * ASAP requires asymmetric cryptography, otherwise we cannot ensure that the client is the exclusive
     * owner of the key used to sign the token. Therefore HMAC algorithms must be rejected. This is in
     * contradiction to <a href="https://tools.ietf.org/html/rfc7518">JWA, sect 3.1</a>,
     * which specifies that HS256 is a "required" algorithm for implementations.
     */
    @Test(expected = UnsupportedAlgorithmException.class)
    public void shouldRejectTokenIfAlgorithmUsesSymmetricCryptography() throws Exception {
        Map<String, Object> headers = add(remove(VALID_HEADERS, "alg"), "alg", "hs256");

        parser.parse(serialisedJwt(headers, VALID_CLAIMS, SIGNATURE));
    }

    @Test(expected = JwtParseException.class)
    public void shouldRejectTokenIfJWtDoesNotHaveThreeParts() throws Exception {
        parser.parse("not-a-jwt");
    }

    @Test(expected = JwtParseException.class)
    public void shouldRejectTokenIfJwtHeadersAreUnparseable() throws Exception {
        String brokenHeaders = Base64.encode("not-a-json-object").toString();
        String validClaims = Base64.encode(JSONObject.toJSONString(VALID_CLAIMS)).toString();
        String irrelevantSignature = SIGNATURE.toString();
        parser.parse(
                String.join(".", brokenHeaders, validClaims, irrelevantSignature));
    }

    @Test(expected = JwtParseException.class)
    public void shouldRejectTokenIfJwtClaimsAreUnparseable() throws Exception {
        String validHeaders = Base64.encode(JSONObject.toJSONString(VALID_HEADERS)).toString();
        String brokenClaims = Base64.encode("not-a-json-object").toString();
        String irrelevantSignature = SIGNATURE.toString();
        parser.parse(
                String.join(".", validHeaders, brokenClaims, irrelevantSignature));
    }

    @Test(expected = JwtParseException.class)
    public void shouldRejectTokenIfPayloadHasUnrepresentableNumbers() throws Exception {
        String header = JSONObject.toJSONString(VALID_HEADERS);
        String payload = "{\"associations\":[],\"sub\":\"59fbaed214be4659f00b1fca\",\"aud\":\"atlassian\",\"impersonation\":[],\"nbf\":1529359725,\"refreshTimeout\":1e29109282,\"iss\":\"session-service\",\"sessionId\":\"94e9d90e-3eca-4613-91da-ce49b0002941\",\"exp\":1531951725,\"iat\":1529359725,\"email\":\"testblarg@shichimitogarashi.org\",\"jti\":\"94e9d90e-3eca-4613-91da-ce49b0002941\"}";
        String signature = VALID_SIGNATURE;

        parser.parse(encodedJwt(header, payload, signature));
    }

    private static String serialisedJwt(Map<String, Object> headers, Map<String, Object> payload, Base64URL signature) {
        return String.join(".", Base64.encode(JSONObject.toJSONString(headers)).toString(),
                Base64.encode(JSONObject.toJSONString(payload)).toString(), signature.toString());
    }

    private static Map<String, Object> add(Map<String, Object> map, String key, @Nullable Object value) {
        HashMap<String, Object> mutableMap = new HashMap<>(map);  // cannot use Guava's because needs to support null values
        mutableMap.put(key, value);
        return Collections.unmodifiableMap(mutableMap);
    }

    private static Map<String, Object> remove(Map<String, Object> map, String key) {
        HashMap<String, Object> mutableMap = new HashMap<>(map);
        mutableMap.remove(key);
        return ImmutableMap.copyOf(mutableMap);
    }

    private static String encodedJwt(String header, String payload, String signature) {
        return String.join(".", Base64.encode(header).toString(), Base64.encode(payload).toString(), Base64.encode(signature).toString());
    }
}
