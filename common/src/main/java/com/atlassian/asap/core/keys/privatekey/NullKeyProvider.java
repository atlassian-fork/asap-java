package com.atlassian.asap.core.keys.privatekey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.validator.ValidatedKeyId;

import java.security.PrivateKey;

/**
 * A provider that provides no keys. Useful as a default provider if none of the other providers is adequate.
 */
public class NullKeyProvider implements KeyProvider<PrivateKey> {
    public static final String URL_SCHEME = "null";

    @Override
    public PrivateKey getKey(ValidatedKeyId keyId) throws CannotRetrieveKeyException {
        throw new CannotRetrieveKeyException("Please configure a private key provider");
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
