package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.Objects;

/**
 * Reads public keys from the filesystem.
 */
public class FilePublicKeyProvider implements KeyProvider<PublicKey> {
    private static final Logger logger = LoggerFactory.getLogger(FilePublicKeyProvider.class);
    private final File baseDirectory;
    private final PemReader pemReader;

    public FilePublicKeyProvider(File baseDirectory, PemReader pemReader) {
        this.baseDirectory = Objects.requireNonNull(baseDirectory);
        this.pemReader = Objects.requireNonNull(pemReader);
    }

    @Override
    public PublicKey getKey(ValidatedKeyId validatedKeyId) throws CannotRetrieveKeyException {
        File file = new File(baseDirectory, validatedKeyId.getKeyId());
        logger.debug("Reading public key from file system: {}", file);

        try (Reader reader = new InputStreamReader(new FileInputStream(file), StandardCharsets.US_ASCII)) {
            return pemReader.readPublicKey(reader);
        } catch (FileNotFoundException e) {
            // log at debug level because this can be caused by invalid input
            logger.debug("Public key file path {} was not found or it is not a file", file);
            throw new PublicKeyNotFoundException(
                    "Public key file path was not found or it is not a file", validatedKeyId, file.toURI());
        } catch (CannotRetrieveKeyException | IOException e) {
            logger.warn("A problem occurred when trying to retrieve public key from file: {}", file, e);
            throw new PublicKeyRetrievalException("Error reading public key from file", validatedKeyId, file.toURI());
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName();
    }
}
