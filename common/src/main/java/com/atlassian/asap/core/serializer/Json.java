package com.atlassian.asap.core.serializer;

import com.google.common.base.Supplier;

import javax.json.spi.JsonProvider;

import static com.google.common.base.Suppliers.memoize;

public class Json {
    private static Supplier<JsonProvider> providerSupplier = memoize(JsonProvider::provider);

    public static JsonProvider provider() {
        return providerSupplier.get();
    }
}
