package com.atlassian.asap.core.exception;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.validator.ValidatedKeyId;

import java.net.URI;

/**
 * Thrown when there was an error communicating with a public key repository.
 *
 * @see PublicKeyNotFoundException
 */
public class PublicKeyRetrievalException extends CannotRetrieveKeyException {
    public PublicKeyRetrievalException(String message, ValidatedKeyId keyId, URI keyUri) {
        super(message, keyId, keyUri);
    }

    public PublicKeyRetrievalException(String message, Throwable cause, ValidatedKeyId keyId, URI keyUri) {
        super(message, cause, keyId, keyUri);
    }
}
