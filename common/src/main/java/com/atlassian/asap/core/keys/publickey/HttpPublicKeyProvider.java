package com.atlassian.asap.core.keys.publickey;

import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.core.exception.PublicKeyNotFoundException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.validator.ValidatedKeyId;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.HttpClientUtils;
import org.apache.http.entity.ContentType;
import org.apache.http.impl.client.DefaultRedirectStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.cache.CacheConfig;
import org.apache.http.impl.client.cache.CachingHttpClientBuilder;
import org.apache.http.impl.client.cache.CachingHttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

import static com.google.common.base.Preconditions.checkArgument;
import static java.lang.String.format;

/**
 * Reads public keys from web servers using the HTTPS protocol.
 */
public class HttpPublicKeyProvider implements KeyProvider<PublicKey> {
    /**
     * The default max connections per route. Note this can still be overridden using http client system property.
     */
    static final int DEFAULT_MAX_CONNECTIONS = 20;
    static final String PEM_MIME_TYPE = "application/x-pem-file";
    static final String ACCEPT_HEADER_VALUE = PEM_MIME_TYPE;

    private static final Logger logger = LoggerFactory.getLogger(HttpPublicKeyProvider.class);

    private final HttpClient httpClient;
    private final PemReader pemReader;
    private final URI baseUrl;

    /**
     * Create a new {@link HttpPublicKeyProvider} instance.
     *
     * @param baseUrl    the base url of the public key server
     * @param httpClient the http client to use for communicating with the public key server
     * @param pemReader  the pem key reader to use for reading public keys in pem format
     */
    public HttpPublicKeyProvider(URI baseUrl, HttpClient httpClient, PemReader pemReader) {
        this(baseUrl, httpClient, pemReader, false);
    }

    HttpPublicKeyProvider(URI baseUrl, HttpClient httpClient, PemReader pemReader, boolean allowInsecureConnections) {
        Objects.requireNonNull(baseUrl, "Base URL cannot be null");
        checkArgument(baseUrl.isAbsolute(), "Base URL must be absolute"); // implies that scheme != null
        checkArgument((allowInsecureConnections && "http".equals(baseUrl.getScheme()))
                || "https".equals(baseUrl.getScheme()), "Invalid base URL scheme");
        checkArgument(StringUtils.endsWith(baseUrl.toString(), "/"), "Base URL does not end with trailing slash: " + baseUrl);

        this.baseUrl = baseUrl;
        this.httpClient = Objects.requireNonNull(httpClient);
        this.pemReader = Objects.requireNonNull(pemReader);
    }

    @Override
    public PublicKey getKey(ValidatedKeyId validatedKeyId) throws CannotRetrieveKeyException {
        URI keyUrl = baseUrl.resolve(validatedKeyId.getKeyId());
        logger.debug("Fetching public key {}, either from the network or from the HTTP client cache", keyUrl);

        HttpResponse response = null;
        try {
            response = httpGetKey(keyUrl);
            int statusCode = response.getStatusLine().getStatusCode();
            switch (statusCode) {
                case HttpStatus.SC_OK:
                    HttpEntity entity = response.getEntity();
                    if (entity != null) {
                        return readEntityAsPublicKey(validatedKeyId, keyUrl, entity);
                    } else {
                        logger.info("Unexpected empty HTTP response when trying to retrieve public key URL {}", keyUrl);
                        throw new PublicKeyRetrievalException("Unexpected empty response getting public key", validatedKeyId, keyUrl);
                    }
                case HttpStatus.SC_FORBIDDEN:
                case HttpStatus.SC_NOT_FOUND:
                    final String statusReason = response.getStatusLine().getReasonPhrase();
                    // log at info level because this can be caused by invalid input
                    logger.info("Public key URL {} returned {} {}", keyUrl, statusCode, statusReason);
                    throw new PublicKeyNotFoundException(format("Encountered %s %s for public key", statusCode, statusReason), validatedKeyId, keyUrl);
                default:
                    logger.warn("Unexpected HTTP status code {} when trying to retrieve public key URL {}", statusCode, keyUrl);
                    throw new PublicKeyRetrievalException("Unexpected status code " + statusCode + " getting public key", validatedKeyId, keyUrl);
            }
        } catch (IOException e) {
            logger.warn("A problem occurred when trying to retrieve public key from URL {}", keyUrl, e);
            throw new PublicKeyRetrievalException("Error getting HTTPS public key - " + e.getMessage(), e, validatedKeyId, keyUrl);
        } finally {
            HttpClientUtils.closeQuietly(response);
        }
    }

    private PublicKey readEntityAsPublicKey(ValidatedKeyId validatedKeyId, URI keyUrl, HttpEntity entity) throws IOException, CannotRetrieveKeyException {
        try (InputStreamReader reader = new InputStreamReader(entity.getContent(), getCharset(entity))) {
            final String mimeType = ContentType.get(entity).getMimeType();
            if (Objects.equals(PEM_MIME_TYPE, mimeType)) {
                return pemReader.readPublicKey(reader);
            } else {
                throw new PublicKeyRetrievalException(
                        format("Unexpected public key MIME type %s, expected %s", mimeType, PEM_MIME_TYPE), validatedKeyId, keyUrl);
            }
        }
    }

    private Charset getCharset(HttpEntity entity) {
        return Optional.ofNullable(ContentType.getOrDefault(entity).getCharset()).orElse(StandardCharsets.US_ASCII);
    }

    private HttpResponse httpGetKey(URI keyUrl) throws IOException {
        HttpGet httpGet = new HttpGet(keyUrl);
        httpGet.setHeader(HttpHeaders.ACCEPT, ACCEPT_HEADER_VALUE);
        return httpClient.execute(httpGet);
    }

    /**
     * Constructs the HTTP client used by the provider if no other {@link HttpClient} is provided. Uses the builder from
     * {@link #defaultHttpClientBuilder()}.
     *
     * @return a new {@link HttpClient}
     */
    public static HttpClient defaultHttpClient() {
        return defaultHttpClientBuilder().build();
    }

    /**
     * Constructs a HTTP client builder with the necessary connection manager and request/cache config.
     *
     * @return a configured {@link HttpClientBuilder}
     * @since 2.21.7
     */
    public static HttpClientBuilder defaultHttpClientBuilder() {
        return configureHttpClientBuilder(CachingHttpClients.custom());
    }

    /**
     * Configures a provided HTTP client builder with the necessary connection manager and request/cache config.
     *
     * @param httpClientBuilder builder to be configured
     * @return a configured {@link HttpClientBuilder}
     * @since 2.11
     */
    public static HttpClientBuilder configureHttpClientBuilder(CachingHttpClientBuilder httpClientBuilder) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
        connectionManager.setDefaultMaxPerRoute(DEFAULT_MAX_CONNECTIONS);
        connectionManager.setMaxTotal(DEFAULT_MAX_CONNECTIONS);

        RequestConfig.Builder requestConfigBuilder = RequestConfig.custom();
        requestConfigBuilder.setConnectionRequestTimeout(((int) TimeUnit.SECONDS.toMillis(5)));
        requestConfigBuilder.setConnectTimeout(((int) TimeUnit.SECONDS.toMillis(5)));
        requestConfigBuilder.setSocketTimeout((int) TimeUnit.SECONDS.toMillis(10));

        CacheConfig cacheConfig = CacheConfig.custom()
                .setMaxCacheEntries(128)
                .setMaxObjectSize(2048) // keys (.pem) are small
                .setHeuristicCachingEnabled(false)
                .setSharedCache(false)
                .setAsynchronousWorkersMax(2)
                .build();

        return httpClientBuilder
                .setCacheConfig(cacheConfig)
                .setDefaultRequestConfig(requestConfigBuilder.build())
                .setConnectionManager(connectionManager)
                .useSystemProperties()
                .setRedirectStrategy(DefaultRedirectStrategy.INSTANCE)
                // assume that the public key repo will be implemented using S3
                .setServiceUnavailableRetryStrategy(new S3ServiceUnavailableRetryStrategy(2, 100));
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + "{baseUrl=" + baseUrl + '}';
    }
}
