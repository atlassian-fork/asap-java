package com.atlassian.asap.api;

/**
 * The header of an ASAP JSON Web Token.
 *
 * @see <a href="https://tools.ietf.org/html/rfc7515">JSON Web Signature</a>
 */
public interface JwsHeader {
    /**
     * @return the value of the 'kid' header. That is, the identifier of the key used to secure the JWT.
     */
    String getKeyId();

    /**
     * @return the value of the 'alg' header. That is, the algorithm used to sign the JWT.
     */
    SigningAlgorithm getAlgorithm();

    /**
     * The headers of a JWT message.
     */
    enum Header {
        ALGORITHM("alg"),
        KEY_ID("kid");

        private final String key;

        Header(String key) {
            this.key = key;
        }

        public String key() {
            return key;
        }
    }
}
