package com.atlassian.asap.api;

/**
 * An ASAP JSON Web Token.
 *
 * @see <a href="https://tools.ietf.org/html/rfc7519">JSON Web Token</a>
 * @see <a href="http://s2sauth.bitbucket.org/">ASAP Authentication</a>
 */
public interface Jwt {
    /**
     * @return the header of the token.
     */
    JwsHeader getHeader();

    /**
     * @return the claims contained in the token payload.
     */
    JwtClaims getClaims();
}
