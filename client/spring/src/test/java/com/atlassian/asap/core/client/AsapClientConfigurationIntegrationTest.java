package com.atlassian.asap.core.client;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@TestPropertySource(properties = {
        "asap.issuer=issuer",
        "asap.key_id=keyId",
        "asap.private_key=data:application/pkcs8;kid=issuer1%2Frsa-key-for-tests%2Fbsales-key1;base64,MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCVQeZrRr5BaQq0KBig0zbk+uDxDX7F047XsChnvseBekcZLY4hcLnCt+X8I9xkxpqyXQiV7EZHDnp4oKIoOjZ7/LeMWEfkDGxE/PsrC1QZpDtpe4rOtp3oKu2cixd28FCf7Uqx7PRqUtmQcG5R4FE8cuP94vanF8T9aMcN0NCDSzUKJqS/LTZte+nQ2FZWiSOmUzefLN3HXFIXva+xkzGSRFHf0HQQG9nJRTHDYXBo1WEBZv4ly6qTRbkrFjWQKs2C/uTZIujG4EckHfebx3WdSIt4LWHixFgCQ5AnZusLWxW/emVq1ZD7NIWhfa81T/5GJdZSwz2la3kOO4wmZXhvAgMBAAECggEABB6yjomAXOvaWMi2jVHnNQDbzquRsUI6sidxphN1DFTku4QvZSroZcCPv5r5zWpSANRilUjVU8bJiKTedP/OWzPJmT0ilt/k6S6WfESuyJpo7Wt0lGokB7GPp3+LYUQW5aM2KZ0hzgGFceA4m1f2xhVhx7NJHamhTWFkBTSbXNRes5Y2WC7zWnSCpSKQOjAkf3GHO+7zC4vWkD9TrNjQfACRKeGS9VjqFbRXPKMALQKKGxzyl2tkq9p2gC83l7b2gSxVn1ZHWDdj4iPOeWfjxSQsvhnl4TrP1I4Fzj2OV99rX7siTaLa844DImLO1Zle6c97THHwqXbyxNf9UCTPGQKBgQDjK6Xm+zUpjfFm0CqwXVH9ok8GofvTy50N4NIjzNvQ0/El3lrjLftt75O43RroiohgbV1Qh2FVtMG7JqL9sJz44zr9/fKABonp1BYLESS8ck/vcaPtV0mVg/oyCqpfHELIs1MrbPlKWZMA/hvshXumSONynZD2U3GSLL73RGorfQKBgQCoMv/UxsHqbX/iSs1wF3J80p/C94FH5Yu9VsbDe+0bSSS7S+7d4N76+cHBN3inSHohCMeh9nCLmbhxr0xVUWULFoTQr1Vjum5oatIz+fTG4/BGy29HGb7R3dRs7x4b72lanJFbkCoGm9V/fMGtHncDVn/KmWz99VbYDRt693l/WwKBgFeMVcab35QhMPyHkzwe6t9NsxkCSlQIb4GOuE21wK1NUO1gMTTGXtSydQLUq28dHIvU+hZz5i4qmFLxA/WzH/vHTs/eE5wFaRcldk1TkQ3THD1SVXO9pQ0rLLwmYhx/frlsaswclkJyHPZP+CECHHIsOwPL97cG14kXF42h3yzdAoGAMF4qD2fwj5dZdRuJpDg55gczhf9in+g0nQf9NG3Iq1YHSAz37boipQ15WDS5b3F6HVHHBp41kw9raNW+H7K+Wcfp/ZDN/1W3NjOxqtywNRmLGPCbWVjf9L3Tvles0t+v7iyj6C62xxu882JO15exJbaUHRCttQH+LTbnth7AQ40CgYEAz3A8IXbefvU/WqhVlsNh7VU383dmKHL5/I55Mm5YnTEGquoil9fc7SCBP8cJOIMqlNPDplApxeTg55Fb2E2C5xi1p7GHbMdVvFV0LQQfF/44FBC2GuGUUcwdM0WUwI3iF+DsyQFvVPrsOvG80B+l7dnrA+s6VHXziqk1fCG6QBI="
})
@ContextConfiguration(classes = {AsapClientConfigurationIntegrationTest.TestContext.class, AsapClientConfiguration.class})
public class AsapClientConfigurationIntegrationTest {
    @Autowired
    private AsapClientConfiguration asapClientConfiguration;

    @Test
    public void shouldGetIssuer() {
        assertThat(asapClientConfiguration.getIssuer(), is("issuer"));
    }

    @Test
    public void shouldGetKeyId() {
        assertThat(asapClientConfiguration.getKeyId(), is("keyId"));
    }

    static class TestContext {
        @Bean
        public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
            // required to expand the ${vars}
            return new PropertySourcesPlaceholderConfigurer();
        }
    }
}

