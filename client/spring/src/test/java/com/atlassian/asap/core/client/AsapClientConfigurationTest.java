package com.atlassian.asap.core.client;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.not;

public class AsapClientConfigurationTest {
    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldFailIfPrivateKeyIsNotParseableAsUri() {
        AsapClientConfiguration asapClientConfiguration = new AsapClientConfiguration("issuer", "keyId");

        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(not(containsString("private key")));

        asapClientConfiguration.privateKeyProvider("not a private key uri");
    }
}
