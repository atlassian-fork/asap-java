package com.atlassian.asap.core.client.spring;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

public class AsapInterceptor implements ClientHttpRequestInterceptor {

    private final Jwt prototype;
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;

    public AsapInterceptor(Jwt prototype, AuthorizationHeaderGenerator authorizationHeaderGenerator) {
        this.prototype = Objects.requireNonNull(prototype);
        this.authorizationHeaderGenerator = Objects.requireNonNull(authorizationHeaderGenerator);
    }

    private String getAsapBearerToken() throws CannotRetrieveKeyException, InvalidTokenException {
        return authorizationHeaderGenerator.generateAuthorizationHeader(newJwtToken());
    }

    private Jwt newJwtToken() {
        Instant now = Instant.now();

        return JwtBuilder.copyJwt(prototype)
                .notBefore(Optional.of(now))
                .issuedAt(now)
                .expirationTime(now.plus(JwtBuilder.DEFAULT_LIFETIME))
                .jwtId(UUID.randomUUID().toString())
                .build();
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        try {
            HttpHeaders requestHeaders = request.getHeaders();
            if (!requestHeaders.containsKey(HttpHeaders.AUTHORIZATION)) {
                requestHeaders.add(HttpHeaders.AUTHORIZATION, getAsapBearerToken());
            }
            return execution.execute(request, body);
        } catch (InvalidTokenException | CannotRetrieveKeyException e) {
            throw new IOException("Exception generating ASAP token.", e);
        }
    }

}
