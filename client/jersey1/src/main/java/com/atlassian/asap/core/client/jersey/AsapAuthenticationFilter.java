package com.atlassian.asap.core.client.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.atlassian.asap.core.client.http.AuthorizationHeaderGeneratorImpl;
import com.google.common.annotations.VisibleForTesting;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientRequest;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.filter.ClientFilter;

import javax.ws.rs.core.HttpHeaders;
import java.net.URI;
import java.time.Instant;
import java.util.Objects;
import java.util.Optional;
import java.util.UUID;

/**
 * Use this class to authenticate Jersey client requests with ASAP.
 */
public class AsapAuthenticationFilter extends ClientFilter {
    private final Jwt prototype;
    private final AuthorizationHeaderGenerator authorizationHeaderGenerator;

    /**
     * @deprecated in 2.2 for removal in 3.0. Use {@link #AsapAuthenticationFilter(Jwt, URI)} instead
     */
    @Deprecated
    public AsapAuthenticationFilter(String audience, String issuer, String keyId, URI privateKeyPath) {
        this(JwtBuilder.newJwt()
                        .audience(audience)
                        .issuer(issuer)
                        .keyId(keyId)
                        .build(),
                AuthorizationHeaderGeneratorImpl.createDefault(privateKeyPath));
    }

    /**
     * @deprecated in 2.2 for removal in 3.0. Use {@link #AsapAuthenticationFilter(Jwt, AuthorizationHeaderGenerator)} instead
     */
    @Deprecated
    public AsapAuthenticationFilter(String audience, String issuer, String keyId, AuthorizationHeaderGenerator authorizationHeaderGenerator) {
        this(JwtBuilder.newJwt()
                        .audience(audience)
                        .issuer(issuer)
                        .keyId(keyId)
                        .build(),
                authorizationHeaderGenerator);
    }

    /**
     * @since 2.2
     */
    public AsapAuthenticationFilter(Jwt prototype, URI privateKeyPath) {
        this(prototype, AuthorizationHeaderGeneratorImpl.createDefault(privateKeyPath));
    }

    /**
     * @since 2.2
     */
    public AsapAuthenticationFilter(Jwt prototype, AuthorizationHeaderGenerator authorizationHeaderGenerator) {
        this.prototype = Objects.requireNonNull(prototype);
        this.authorizationHeaderGenerator = Objects.requireNonNull(authorizationHeaderGenerator);
    }

    @Override
    public final ClientResponse handle(ClientRequest clientRequest) throws ClientHandlerException {
        // Due to the way in which Jersey chains its filters (it uses a package-protected setNext in the
        // parent class), we cannot control the next filter for the tests. Therefore we extract our logic
        // to handleInternal and keep the chaining in this (untested) method.
        handleInternal(clientRequest);
        return getNext().handle(clientRequest);
    }

    @VisibleForTesting
    void handleInternal(ClientRequest clientRequest) throws ClientHandlerException {
        if (!clientRequest.getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
            try {
                clientRequest.getHeaders().add(HttpHeaders.AUTHORIZATION, getAsapBearerToken());
            } catch (CannotRetrieveKeyException | InvalidTokenException ex) {
                throw new ClientHandlerException("Exception generating ASAP token.", ex);
            }
        }
    }

    private String getAsapBearerToken() throws CannotRetrieveKeyException, InvalidTokenException {
        return authorizationHeaderGenerator.generateAuthorizationHeader(newJwtToken());
    }

    private Jwt newJwtToken() {
        Instant now = Instant.now();

        // We may want to modify the JwtId in the future. For now we just update the expiration details
        return JwtBuilder.copyJwt(prototype)
                .notBefore(Optional.of(now))
                .issuedAt(now)
                .expirationTime(now.plus(JwtBuilder.DEFAULT_LIFETIME))
                .jwtId(UUID.randomUUID().toString())
                .build();
    }
}
