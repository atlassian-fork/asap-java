package com.atlassian.asap.core.client.http;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import com.google.common.base.Throwables;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.util.concurrent.UncheckedExecutionException;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import static com.google.common.base.Preconditions.checkArgument;
import static java.util.Objects.requireNonNull;

/**
 * A caching implementation of {@link AuthorizationHeaderGenerator}.
 *
 * <p>The implementation uses a fixed size cache of size {@code maxCacheSize} which evicts entries based on
 * least recently used. Tokens are not reused within the {@code expiryBuffer} duration of the expiry time,
 * minimising the chance of an expired token being used due to race conditions. JWTs with custom claims or
 * expires bypass the cache. cached tokens have their expiry modified to be {@code expiry} in duration.
 */
public class CachingAuthorizationHeaderGenerator implements AuthorizationHeaderGenerator {

    /**
     * The default size of the cache.
     *
     * @see #CachingAuthorizationHeaderGenerator(AuthorizationHeaderGenerator)
     */
    @SuppressWarnings("WeakerAccess")
    public static final int DEFAULT_CACHE_SIZE = 50;
    /**
     * The default expiry duration for the JWT token.
     *
     * @see #CachingAuthorizationHeaderGenerator(AuthorizationHeaderGenerator)
     */
    @SuppressWarnings("WeakerAccess")
    public static final Duration DEFAULT_EXPIRY = Duration.ofMinutes(10);
    /**
     * The default expiry buffer for cached tokens.
     *
     * @see #CachingAuthorizationHeaderGenerator(AuthorizationHeaderGenerator)
     */
    @SuppressWarnings("WeakerAccess")
    public static final Duration DEFAULT_EXPIRY_BUFFER = Duration.ofMinutes(1);

    private static final Set<String> REGISTERED_CLAIMS = Collections.unmodifiableSet(
            Arrays.stream(JwtClaims.RegisteredClaim.values())
                    .map(JwtClaims.RegisteredClaim::key)
                    .collect(Collectors.toSet())
    );

    private final AuthorizationHeaderGenerator delegate;
    private final Clock clock;
    private final Duration expiry;
    private final Duration expiryBuffer;

    private final Cache<JwtTemplate, CachedToken> tokenCache;

    /**
     * Construct a {@link CachingAuthorizationHeaderGenerator} using the
     * default cache size, expiry and expiry buffer.
     *
     * @param delegate the {@link AuthorizationHeaderGenerator} to delegate to
     */
    @SuppressWarnings("unused")
    public CachingAuthorizationHeaderGenerator(AuthorizationHeaderGenerator delegate) {
        this(delegate, Clock.systemUTC(), DEFAULT_CACHE_SIZE, DEFAULT_EXPIRY, DEFAULT_EXPIRY_BUFFER);
    }

    /**
     * Construct a {@link CachingAuthorizationHeaderGenerator}.
     *
     * @param delegate     the {@link AuthorizationHeaderGenerator} to delegate to
     * @param clock        the clock to use for determining cached entry freshness
     * @param maxCacheSize the maximum number of entries permitted in the
     *                     cache after which entries are evicted based on least recently used
     * @param expiry       the duration the resulting JWT token is valid for
     * @param expiryBuffer the duration of time under which a cached token is considered invalid in the cache and
     *                     should be replaced however the JWT token itself may still be valid
     */
    @SuppressWarnings("WeakerAccess")
    public CachingAuthorizationHeaderGenerator(AuthorizationHeaderGenerator delegate,
                                               Clock clock,
                                               int maxCacheSize,
                                               Duration expiry,
                                               Duration expiryBuffer) {
        this.delegate = requireNonNull(delegate);
        this.clock = requireNonNull(clock);
        this.expiry = requireNonNull(expiry);
        this.expiryBuffer = requireNonNull(expiryBuffer);
        checkArgument(expiryBuffer.minus(expiry).isNegative(), "Expiry must be larger than the expiry buffer");

        this.tokenCache = CacheBuilder.newBuilder()
                .maximumSize(maxCacheSize)
                .build();
    }

    @Override
    public String generateAuthorizationHeader(Jwt jwt) throws InvalidTokenException, CannotRetrieveKeyException {
        if (!isSuitableForReuse(jwt)) {
            return delegate.generateAuthorizationHeader(jwt);
        }

        JwtTemplate template = JwtTemplate.fromJwt(jwt);
        TokenCreator creator = () -> {
            Instant now = clock.instant();
            String value = delegate.generateAuthorizationHeader(template.toJwt(now, expiry));
            return new CachedToken(now.plus(expiry).minus(expiryBuffer), value);
        };

        CachedToken token;
        try {
            token = tokenCache.get(template, creator);
        } catch (ExecutionException | UncheckedExecutionException e) {
            Throwables.propagateIfInstanceOf(e.getCause(), InvalidTokenException.class);
            Throwables.propagateIfInstanceOf(e.getCause(), CannotRetrieveKeyException.class);
            Throwables.propagateIfPossible(e.getCause());
            throw Throwables.propagate(e);
        }

        if (isRequiringRefresh(token)) {
            token = creator.call();
            tokenCache.put(template, token);
        }

        return token.value;
    }

    private boolean isSuitableForReuse(Jwt jwt) {
        return jwt.getHeader().getAlgorithm() == JwtBuilder.DEFAULT_ALGORITHM &&
                JwtBuilder.DEFAULT_LIFETIME.equals(Duration.between(jwt.getClaims().getIssuedAt(), jwt.getClaims().getExpiry())) &&
                REGISTERED_CLAIMS.containsAll(jwt.getClaims().getJson().keySet());
    }

    private boolean isRequiringRefresh(CachedToken token) {
        return token == null || clock.instant().isAfter(token.expiry);
    }

    @FunctionalInterface
    interface TokenCreator extends Callable<CachedToken> {
        @Override
        CachedToken call() throws InvalidTokenException, CannotRetrieveKeyException;
    }

    private static class JwtTemplate {
        private final String keyId;
        private final String issuer;
        private final Optional<String> subject;
        private final Set<String> audience;

        JwtTemplate(String keyId, String issuer, Optional<String> subject, Set<String> audience) {
            this.keyId = keyId;
            this.issuer = issuer;
            this.subject = subject;
            this.audience = audience;
        }

        static JwtTemplate fromJwt(Jwt jwt) {
            return new JwtTemplate(
                    jwt.getHeader().getKeyId(),
                    jwt.getClaims().getIssuer(),
                    jwt.getClaims().getSubject(),
                    jwt.getClaims().getAudience()
            );
        }

        Jwt toJwt(Instant issuedAt, Duration expiry) {
            return JwtBuilder.newJwt()
                    .keyId(keyId)
                    .issuer(issuer)
                    .subject(subject)
                    .audience(audience)
                    .notBefore(Optional.of(issuedAt))
                    .issuedAt(issuedAt)
                    .expirationTime(issuedAt.plus(expiry))
                    .build();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            JwtTemplate that = (JwtTemplate) o;
            return keyId.equals(that.keyId) &&
                    issuer.equals(that.issuer) &&
                    subject.equals(that.subject) &&
                    audience.equals(that.audience);
        }

        @Override
        public int hashCode() {
            return Objects.hash(keyId, issuer, subject, audience);
        }
    }

    private static class CachedToken {
        private final Instant expiry;
        private final String value;

        CachedToken(Instant expiry, String value) {
            this.expiry = expiry;
            this.value = value;
        }
    }

}
