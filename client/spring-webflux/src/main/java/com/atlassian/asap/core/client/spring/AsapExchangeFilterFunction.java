package com.atlassian.asap.core.client.spring;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.JwtBuilder;
import com.atlassian.asap.api.client.http.AuthorizationHeaderGenerator;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;
import org.springframework.http.HttpHeaders;
import org.springframework.web.reactive.function.client.ClientRequest;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeFunction;
import reactor.core.publisher.Mono;
import java.io.IOException;

/**
 * Function that adds an ASAP bearer token to the request Authorization Header, if none is present.
 */
public class AsapExchangeFilterFunction implements ExchangeFilterFunction {

  private final AuthorizationHeaderGenerator authorizationHeaderGenerator;
  private final Jwt prototype;

  public AsapExchangeFilterFunction(
      AuthorizationHeaderGenerator authorizationHeaderGenerator,
      Jwt prototype) {
    this.authorizationHeaderGenerator = authorizationHeaderGenerator;
    this.prototype = prototype;
  }

  @Override
  public Mono<ClientResponse> filter(final ClientRequest request, final ExchangeFunction next) {
    if (request.headers().get(HttpHeaders.AUTHORIZATION) != null) {
      return next.exchange(request);
    }

    final ClientRequest.Builder newRequestBuilder = ClientRequest.from(request);
    final Jwt jwt = JwtBuilder.newFromPrototype(prototype).build();
    try {
      newRequestBuilder.header(HttpHeaders.AUTHORIZATION, authorizationHeaderGenerator
          .generateAuthorizationHeader(jwt));

      return next.exchange(newRequestBuilder.build());
    } catch (CannotRetrieveKeyException | InvalidTokenException e) {
      return Mono.error(new IOException("Exception generating ASAP token.", e));
    }
  }
}
