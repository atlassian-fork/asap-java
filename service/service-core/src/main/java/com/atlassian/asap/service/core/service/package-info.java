/**
 * Default implementations for the core services published by the service API.
 */
@FieldsAreNonNullByDefault
@ParametersAreNonnullByDefault
@ReturnTypesAreNonNullByDefault

package com.atlassian.asap.service.core.service;

import com.atlassian.asap.annotation.FieldsAreNonNullByDefault;
import com.atlassian.asap.annotation.ReturnTypesAreNonNullByDefault;

import javax.annotation.ParametersAreNonnullByDefault;
