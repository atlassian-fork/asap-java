package com.atlassian.asap.service.api;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.service.api.TokenValidator.Policy;

import java.util.Optional;

/**
 * The result from evaluating an {@code Authorization} header against a {@link TokenValidator}.
 *
 * @since 2.8
 */
public interface ValidationResult {
    /**
     * The overall decision as to whether or not the {@code Authorization} header has satisfied the
     * validator's requirements.
     *
     * @return the decision as to whether or not the request is authorized
     */
    Decision decision();

    /**
     * The token, if any, that was used to authorize this request.
     * <p>
     * Note: The value will only be present when {@link #decision()} is {@link Decision#AUTHORIZED}.
     * </p>
     *
     * @return the token, if any, that was used to satisfy the authorization requirements
     */
    Optional<Jwt> token();

    /**
     * The issuer, if any, that provided a token to the request.
     * <p>
     * <strong>WARNING</strong>: This is the issuer that the token claimed to be from, but since the token
     * has been rejected, this information <strong>MUST NOT</strong> be trusted.  It is provided for
     * information / logging purposes, only.
     * </p>
     *
     * @return the issuer, if any, that an untrusted token claimed as its issuer
     */
    Optional<String> untrustedIssuer();


    /**
     * The overall decision as to whether or not the request has satisfied the validator's requirements.
     * <p>
     * <strong>Usage Note</strong>
     * </p>
     * <p>
     * The names used for the {@code Decision} values and their documentation reflect the standard use of the
     * terms "authentication" and "authorization" in the security industry.  Specifically, "authentication" is
     * about <strong>identification</strong> (proving who you are) and "authorization" is about
     * <strong>permission</strong> (associating who you are with what you are allowed to do).
     * </p><p>
     * In the early days of HTTP, "authentication" and "authorization" were largely synonymous because the access
     * controls were so simple that no distinction existed between these two concepts.  An HTML page was
     * restricted with a username and password, and if you were able to successfully authenticate, then that
     * implicitly meant that you were authorized to access its content.  Through this unfortunate accident of
     * history, the HTTP protocol calls the {@code 401} status {@code "Unauthorized"}.  Semantically, however, a
     * <code>"401&nbsp;Unauthorized"</code> status really means that the client is <strong>not authenticated</strong>,
     * and a <code>"403&nbsp;Forbidden"</code> status means that the client is <strong>authenticated but not
     * authorized</strong>.
     * </p>
     */
    enum Decision {
        /**
         * ASAP authentication was attempted, but the {@code Authorization} header was missing, could not
         * be parsed as containing a valid token, or is otherwise unacceptable.
         * <p>
         * Example reasons for this result include:
         * </p>
         * <ul>
         * <li>The enforcement policy is {@link Policy#REQUIRE REQUIRE}, but the {@code Authorization} header is
         * missing or does not contain a valid JWT.</li>
         * <li>The token's issuer does not match its key ID.</li>
         * <li>The token's audience is not valid (the token is meant for somebody else, not us).</li>
         * <li>The token has invalid time information, such as claiming to have been created far in the future.</li>
         * <li>The token has expired.</li>
         * </ul>
         * <p>
         * Expected HTTP Status: {@code 401 Unauthorized}  (See the {@link Decision Usage Note})
         * </p>
         */
        NOT_AUTHENTICATED,

        /**
         * ASAP authentication is required, but no matching public key could be located to verify it.
         * <p>
         * This could be either a temporary problem accessing the key service or a permanent failure if the key
         * does not exist.  Either way, since the authenticity of the token could not be verified, its contents
         * cannot be trusted.
         * </p>
         * <p>
         * Expected HTTP Status: {@code 401 Unauthorized}  (See the {@link Decision Usage Note})
         * </p>
         */
        NOT_VERIFIED,

        /**
         * ASAP authentication was attempted by the client, but at least one of the validation constraints could
         * not be satisfied (for example, if the token is not from an issuer that is authorized to use this resource).
         * <p>
         * Expected HTTP Status: {@code 403 Forbidden}
         * </p>
         */
        NOT_AUTHORIZED,

        /**
         * ASAP authentication was successful, and the token satisfied all of the validator's authorization
         * requirements.
         */
        AUTHORIZED,

        /**
         * ASAP authentication is explicitly disallowed, but an authentication header specifying an ASAP token was
         * found.
         * <p>
         * Note that since the {@link Policy#REJECT REJECT} policy refuses all ASAP tokens regardless of their
         * contents, tokens are not passed through their normal validity checks.  Any token received is assumed
         * to pass authentication and rejected as unauthorized, instead.
         * </p><p>
         * Expected HTTP Status: {@code 403 Forbidden}
         * </p>
         */
        REJECTED,

        /**
         * ASAP authentication was not used for this request.
         * <p>
         * One of the following conditions applies:
         * </p>
         * <ul>
         * <li>The policy is {@link Policy#IGNORE IGNORE} and it is unknown whether any token may have been
         * provided, because the {@code Authorization} header was not examined.</li>
         * <li>The policy is {@link Policy#OPTIONAL OPTIONAL} and no token was provided.</li>
         * <li>The policy is {@link Policy#REJECT REJECT} and no token was provided.</li>
         * </ul>
         */
        ABSTAIN;

        /**
         * If {@code true}, then the validation was successful (either an appropriate token was found or none was
         * needed).
         *
         * @return {@code true} if this decision allows a request to proceed
         */
        public boolean isOk() {
            return this == AUTHORIZED || this == ABSTAIN;
        }
    }
}

