package com.atlassian.asap.core.validator;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.CannotRetrieveKeyException;
import com.atlassian.asap.api.exception.InvalidTokenException;

import java.util.Optional;

/**
 * Interface for parsing, verifying and validating a {@link Jwt} token.
 */
public interface JwtValidator {
    /**
     * Parses the encoded JWT message from {@link String}, verifies its signature, validates its claims and on success
     * returns the decoded {@link Jwt}.
     *
     * @param serializedJwt a JSON Web Token
     * @return a signature verified and syntactically valid {@link Jwt}
     * @throws InvalidTokenException      if the JWT string was malformed (see subclasses)
     * @throws CannotRetrieveKeyException if the public key to verify the signature of the JWT can't be retrieved
     */
    Jwt readAndValidate(String serializedJwt)
            throws InvalidTokenException, CannotRetrieveKeyException;

    /**
     * Extracts the issuer, if at all possible, from the claims section by parsing the given serialized JWT.
     * This will NOT validate anything in the JWT, and is only intended to be used to assist in returning useful
     * authentication failure messages to clients, not for real issuer validation.
     *
     * @param serializedJwt a JSON Web Token
     * @return the issuer claim from that JWT, or else empty if none could be parsed or found
     */
    Optional<String> determineUnverifiedIssuer(String serializedJwt);
}
