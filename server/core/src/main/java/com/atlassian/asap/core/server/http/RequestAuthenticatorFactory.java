package com.atlassian.asap.core.server.http;

import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;


/**
 * Default RequestAuthenticatorFactory which passes the auth context to JwtValidator as is. May be overridden in projects.
 */
public class RequestAuthenticatorFactory {
    public RequestAuthenticator create(AuthenticationContext authContext) {
        JwtValidator jwtValidator = JwtValidatorImpl.createDefault(authContext);
        return new RequestAuthenticatorImpl(jwtValidator);
    }
}
