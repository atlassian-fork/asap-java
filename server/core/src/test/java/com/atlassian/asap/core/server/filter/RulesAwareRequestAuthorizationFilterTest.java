package com.atlassian.asap.core.server.filter;

import com.atlassian.asap.api.Jwt;
import com.google.common.collect.ImmutableMap;
import org.junit.Test;

import java.util.Map;
import java.util.function.Predicate;

import static com.atlassian.asap.core.server.filter.IssuerAndSubjectAwareRequestAuthorizationFilterTest.getJwt;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

public class RulesAwareRequestAuthorizationFilterTest {
    @Test
    public void issuerCanBeAuthorized() throws Exception {
        Map<String, Predicate<Jwt>> rules = ImmutableMap.of("good/issuer", (jwt) -> true);
        RulesAwareRequestAuthorizationFilter filter = new RulesAwareRequestAuthorizationFilter(rules);
        Jwt jwt = getJwt("good/issuer", null);

        assertThat(filter.isAuthorized(null, jwt), is(true));
    }

    @Test
    public void issuerCanBeRejected() throws Exception {
        Map<String, Predicate<Jwt>> rules = ImmutableMap.of("bad/issuer", (jwt) -> false);
        RulesAwareRequestAuthorizationFilter filter = new RulesAwareRequestAuthorizationFilter(rules);
        Jwt jwt = getJwt("bad/issuer", null);

        assertThat(filter.isAuthorized(null, jwt), is(false));
    }

    @Test
    public void unlistedIssuerIsRejected() throws Exception {
        Map<String, Predicate<Jwt>> rules = ImmutableMap.of("good/issuer", (jwt) -> true);
        RulesAwareRequestAuthorizationFilter filter = new RulesAwareRequestAuthorizationFilter(rules);
        Jwt jwt = getJwt("bad/issuer", null);

        assertThat(filter.isAuthorized(null, jwt), is(false));
    }

    @Test
    public void correctJwtIsPassedThrough() throws Exception {
        Jwt jwt = getJwt("good/issuer", null);

        Map<String, Predicate<Jwt>> rules = ImmutableMap.of("good/issuer", (passedJwt) -> {
            assert passedJwt.equals(jwt);
            return true;
        });
        new RulesAwareRequestAuthorizationFilter(rules).isAuthorized(null, jwt);
    }
}
