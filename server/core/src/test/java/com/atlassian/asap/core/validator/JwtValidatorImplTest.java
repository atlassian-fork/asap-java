package com.atlassian.asap.core.validator;

import com.atlassian.asap.api.JwsHeader;
import com.atlassian.asap.api.JwtClaims;
import com.atlassian.asap.api.SigningAlgorithm;
import com.atlassian.asap.core.exception.InvalidClaimException;
import com.atlassian.asap.core.exception.JwtParseException;
import com.atlassian.asap.core.exception.PublicKeyRetrievalException;
import com.atlassian.asap.core.exception.SignatureMismatchException;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.parser.JwtParser;
import com.atlassian.asap.core.parser.VerifiableJwt;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.security.PublicKey;
import java.util.Collections;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JwtValidatorImplTest {
    private static final String ISSUER = "issuer";
    private static final String SERIALIZED_JWT = "some serialized jwt";
    private static final String RESOURCE_SERVER_AUDIENCE = "theResourceServerAudience";

    @Mock
    private KeyProvider<PublicKey> publicKeyProvider;
    @Mock
    private JwtParser jwtParser;
    @Mock
    private VerifiableJwt jwt;
    @Mock
    private JwsHeader jwsHeader;
    @Mock
    private JwtClaims jwtClaims;
    @Mock
    private PublicKey publicKey;
    @Mock
    private JwtClaimsValidator jwtClaimsValidator;

    private JwtValidator jwtValidator;

    @Before
    public void setUp() throws Exception {
        when(jwsHeader.getAlgorithm()).thenReturn(SigningAlgorithm.RS256);
        when(jwt.getHeader()).thenReturn(jwsHeader);
        when(jwt.getClaims()).thenReturn(jwtClaims);
        when(jwsHeader.getKeyId()).thenReturn("public-key");

        jwtValidator = new JwtValidatorImpl(
                publicKeyProvider,
                jwtParser,
                jwtClaimsValidator,
                RESOURCE_SERVER_AUDIENCE
        );
    }

    @Test(expected = JwtParseException.class)
    public void shouldFailIfJwtUnparseable() throws Exception {
        when(jwtParser.parse(SERIALIZED_JWT)).thenThrow(new JwtParseException("Can't parse jwt"));
        jwtValidator.readAndValidate(SERIALIZED_JWT);
    }

    @Test(expected = PublicKeyRetrievalException.class)
    public void shouldFailIfPublicKeyNotRetrievable() throws Exception {
        String issuer = "an issuer without a public key";
        when(jwt.getClaims().getIssuer()).thenReturn(issuer);

        when(jwtParser.parse(SERIALIZED_JWT)).thenReturn(jwt);
        final ValidatedKeyId validatedKeyId = ValidatedKeyId.validate("public-key");
        when(publicKeyProvider.getKey(validatedKeyId)).thenThrow(new PublicKeyRetrievalException("Can't retrieve public key", validatedKeyId, null));

        jwtValidator.readAndValidate(SERIALIZED_JWT);
    }

    @Test
    public void shouldFailIfJwtNotVerified() throws Exception {
        String issuer = "issuer";

        when(jwt.getClaims().getIssuer()).thenReturn(issuer);
        when(publicKeyProvider.getKey(ValidatedKeyId.validate("public-key"))).thenReturn(publicKey);
        when(jwtParser.parse(SERIALIZED_JWT)).thenReturn(jwt);
        doThrow(new SignatureMismatchException("Couldn't verifySignature jwt")).when(jwt).verifySignature(publicKey);

        try {
            jwtValidator.readAndValidate(SERIALIZED_JWT);
            fail("Should have thrown");
        } catch (SignatureMismatchException ex) {
            verify(jwt).verifySignature(publicKey);
        }
    }

    @Test(expected = InvalidClaimException.class)
    public void shouldFailIfClaimsMismatch() throws Exception {
        String issuer = "issuer";

        when(jwt.getClaims().getIssuer()).thenReturn(issuer);
        when(publicKeyProvider.getKey(ValidatedKeyId.validate("public-key"))).thenReturn(publicKey);
        when(jwtParser.parse(SERIALIZED_JWT)).thenReturn(jwt);
        doThrow(new InvalidClaimException(JwtClaims.RegisteredClaim.JWT_ID, "Bad Claim"))
                .when(jwtClaimsValidator).validate(jwt, Collections.singleton(RESOURCE_SERVER_AUDIENCE));

        jwtValidator.readAndValidate(SERIALIZED_JWT);
    }

    @Test
    public void shouldReturnTrueIfVerificationSucceeds() throws Exception {
        String issuer = "an issuer";

        when(jwt.getClaims().getIssuer()).thenReturn(issuer);
        when(publicKeyProvider.getKey(ValidatedKeyId.validate(jwsHeader.getKeyId()))).thenReturn(publicKey);
        when(jwtParser.parse(SERIALIZED_JWT)).thenReturn(jwt);

        assertEquals(jwt, jwtValidator.readAndValidate(SERIALIZED_JWT));
        verify(jwt).verifySignature(publicKey);
        verify(jwtClaimsValidator).validate(jwt, Collections.singleton(RESOURCE_SERVER_AUDIENCE));
    }

    @Test
    public void shouldDelegateDetermineIssuerToParser() {
        when(jwtParser.determineUnverifiedIssuer(SERIALIZED_JWT)).thenReturn(Optional.of(ISSUER));

        Optional<String> issuer = jwtValidator.determineUnverifiedIssuer(SERIALIZED_JWT);

        assertEquals(ISSUER, issuer.get());
    }
}
