package com.atlassian.asap.it;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthenticationFailedException;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.server.AuthenticationContext;
import com.atlassian.asap.core.server.http.RequestAuthenticatorFactory;
import com.atlassian.asap.core.server.http.RequestAuthenticatorImpl;
import com.atlassian.asap.core.server.jersey.JerseyRequestAuthorizer;
import com.atlassian.asap.core.server.jersey.JerseyRequestAuthorizerFactory;
import com.atlassian.asap.core.server.jersey.JwtAuth;
import com.atlassian.asap.core.server.jersey.JwtAuthProvider;
import com.atlassian.asap.core.server.jersey.JwtInjectable;
import com.atlassian.asap.core.server.jersey.WhitelistJerseyRequestAuthorizer;
import com.google.common.collect.ImmutableMap;
import com.sun.jersey.api.core.ClassNamesResourceConfig;
import com.sun.jersey.api.core.HttpRequestContext;
import com.sun.jersey.api.core.ResourceConfig;
import com.sun.jersey.core.spi.component.ComponentContext;
import com.sun.jersey.core.spi.component.ComponentScope;
import com.sun.jersey.core.spi.component.ioc.IoCComponentProviderFactory;
import com.sun.jersey.server.impl.application.WebApplicationImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JwtValidatorInitialisationTest {

    private static final String DUMMY_ISSUER = "dummy";

    @Mock
    private JwtAuth mockJwtAuth;

    @Test
    public void shouldUseDefaultFactories() throws Exception {
        Map<Class, Object> injectables = ImmutableMap.<Class, Object>of(
                AuthenticationContext.class, new AuthenticationContext(DUMMY_ISSUER, "file:///some/path")
        );
        IoCComponentProviderFactory noFactoryIoCProvider = JerseyTestUtil.ioCProviderFactoryfromMap(injectables);

        JwtInjectable jwtInjectable = getJwtInjectable(noFactoryIoCProvider);

        assertNotNull(jwtInjectable.getRequestAuthenticator());
        assertEquals(RequestAuthenticatorImpl.class, jwtInjectable.getRequestAuthenticator().getClass());

        assertNotNull(jwtInjectable.getJerseyRequestAuthorizer());
        assertEquals(WhitelistJerseyRequestAuthorizer.class, jwtInjectable.getJerseyRequestAuthorizer().getClass());
    }

    @Test
    public void shouldWireCustomFactoriesIfAvailableInTheContext() throws Exception {
        Map<Class, Object> injectables = ImmutableMap.<Class, Object>of(
                AuthenticationContext.class, new AuthenticationContext(DUMMY_ISSUER, "file:///some/path"),
                RequestAuthenticatorFactory.class, new CustomRequestAuthenticatorFactory(),
                JerseyRequestAuthorizerFactory.class, new CustomJerseyRequestAuthorizerFactory()
        );
        IoCComponentProviderFactory customFactoryIoCProvider = JerseyTestUtil.ioCProviderFactoryfromMap(injectables);

        JwtInjectable jwtInjectable = getJwtInjectable(customFactoryIoCProvider);

        assertNotNull(jwtInjectable.getRequestAuthenticator());
        assertEquals(CustomRequestAuthenticator.class, jwtInjectable.getRequestAuthenticator().getClass());

        assertNotNull(jwtInjectable.getJerseyRequestAuthorizer());
        assertEquals(CustomJerseyRequestAuthorizer.class, jwtInjectable.getJerseyRequestAuthorizer().getClass());
    }

    private JwtInjectable getJwtInjectable(IoCComponentProviderFactory ioCComponentProviderFactory) throws IllegalAccessException {
        WebApplicationImpl webApplication = createWebApplication(ioCComponentProviderFactory);

        when(mockJwtAuth.authorizedSubjects()).thenReturn(new String[]{DUMMY_ISSUER});
        when(mockJwtAuth.authorizedIssuers()).thenReturn(new String[]{});

        return (JwtInjectable) (webApplication.getServerInjectableProviderFactory()
                .getInjectable(JwtAuth.class, mock(ComponentContext.class), mockJwtAuth, Jwt.class, ComponentScope.PerRequest));
    }

    private WebApplicationImpl createWebApplication(IoCComponentProviderFactory ioCComponentProviderFactory) {
        ResourceConfig rc = new ClassNamesResourceConfig(
                JwtAuthProvider.class,
                Controller.class
        );
        rc.getSingletons().add(ioCComponentProviderFactory);
        WebApplicationImpl webApplication = new WebApplicationImpl();
        webApplication.initiate(rc);

        return webApplication;
    }

    public class CustomRequestAuthenticatorFactory extends RequestAuthenticatorFactory {
        @Override
        public RequestAuthenticator create(AuthenticationContext authContext) {
            return new CustomRequestAuthenticator();
        }
    }

    public class CustomRequestAuthenticator implements RequestAuthenticator {
        @Override
        public Jwt authenticateRequest(String authorizationHeader) throws AuthenticationFailedException {
            return mock(Jwt.class);
        }
    }

    public class CustomJerseyRequestAuthorizerFactory extends JerseyRequestAuthorizerFactory {
        @Override
        public JerseyRequestAuthorizer create(JwtAuth jwtAuth) {
            return new CustomJerseyRequestAuthorizer();
        }
    }

    public class CustomJerseyRequestAuthorizer implements JerseyRequestAuthorizer {

        @Override
        public void authorize(Jwt authenticJwt, HttpRequestContext requestContext) throws AuthorizationFailedException {
            // do nothing
        }
    }

    @Path("/")
    public static class Controller {
        @GET
        public String resourceIssuer1(
                @JwtAuth(authorizedSubjects = {"NOBODY"}) Jwt jwt
        ) {
            return "OK";
        }
    }

}
