package com.atlassian.asap.core.server.jersey;

import com.atlassian.asap.api.Jwt;
import com.atlassian.asap.api.exception.AuthorizationFailedException;
import com.sun.jersey.api.core.HttpRequestContext;

/**
 * Authorization strategy for Jersey requests.
 */
public interface JerseyRequestAuthorizer {
    /**
     * Tries to authorize an authentic JWT token to perform a request.
     *
     * @param authenticJwt   an authentic JWT token
     * @param requestContext the request that is to be authorised
     * @throws AuthorizationFailedException if the token is not authorized for this request
     */
    void authorize(Jwt authenticJwt, HttpRequestContext requestContext) throws AuthorizationFailedException;
}
