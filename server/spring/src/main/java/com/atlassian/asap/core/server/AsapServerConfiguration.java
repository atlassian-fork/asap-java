package com.atlassian.asap.core.server;

import com.atlassian.asap.api.server.http.RequestAuthenticator;
import com.atlassian.asap.core.keys.KeyProvider;
import com.atlassian.asap.core.keys.PemReader;
import com.atlassian.asap.core.keys.publickey.HttpPublicKeyProvider;
import com.atlassian.asap.core.keys.publickey.PublicKeyProviderFactory;
import com.atlassian.asap.core.parser.JwtParser;
import com.atlassian.asap.core.server.http.RequestAuthenticatorImpl;
import com.atlassian.asap.core.validator.JwtClaimsValidator;
import com.atlassian.asap.core.validator.JwtValidator;
import com.atlassian.asap.core.validator.JwtValidatorImpl;
import com.atlassian.asap.nimbus.parser.NimbusJwtParser;
import com.google.common.annotations.VisibleForTesting;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.HttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

import java.security.PublicKey;
import java.time.Clock;
import java.util.Collections;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Server-side ASAP configuration.
 */
@Configuration
public class AsapServerConfiguration {
    private static final Pattern CSV_PATTERN = Pattern.compile(",");

    private final String audience;

    private final String audienceOverride;

    private final String publicKeyRepositoryAdditionalUrl;


    /**
     * @param audience                         the single audience this server would be responding to
     * @param audienceOverride                 if non-blank, this CSV set of audiences will take precedence over the single audience
     * @param publicKeyRepositoryAdditionalUrl if non-blank, this provides an additional public key repository
     */
    @Autowired
    AsapServerConfiguration(@Value("${asap.audience}") String audience,
                            @Value("${asap.audience_override:}") String audienceOverride,
                            @Value("${asap.public_key_repository.additional_url:}") String publicKeyRepositoryAdditionalUrl) {
        this.audience = audience;
        this.audienceOverride = audienceOverride;
        this.publicKeyRepositoryAdditionalUrl = publicKeyRepositoryAdditionalUrl;
    }

    /**
     * Definition of the {@link JwtValidator} bean.
     *
     * @param publicKeyProvider  the public key provider to look up public keys
     * @param jwtParser          the parser to use for JWT token parsing
     * @param jwtClaimsValidator the validator for the claims in the JWT token
     * @return a token validator for the given audience and public key repository
     */
    @Bean
    public JwtValidator jwtValidator(KeyProvider<PublicKey> publicKeyProvider,
                                     JwtParser jwtParser,
                                     JwtClaimsValidator jwtClaimsValidator) {
        return new JwtValidatorImpl(publicKeyProvider, jwtParser, jwtClaimsValidator, getAllAudiences());
    }

    Set<String> getAllAudiences() {
        if (StringUtils.isBlank(audienceOverride)) {
            return Collections.singleton(audience);
        } else {
            return CSV_PATTERN.splitAsStream(audienceOverride)
                    .map(String::trim)
                    .filter(StringUtils::isNotBlank)
                    .collect(Collectors.toSet());
        }
    }

    /**
     * Definition of the {@link JwtClaimsValidator} bean.
     *
     * @return a validator of JWT claims
     */
    @Bean
    public JwtClaimsValidator jwtClaimsValidator() {
        return new JwtClaimsValidator(Clock.systemUTC());
    }

    /**
     * Definition of the {@link JwtParser} bean.
     *
     * @return a parser of JWT tokens
     */
    @Bean
    public JwtParser jwtParser() {
        return new NimbusJwtParser();
    }

    /**
     * Definition of the {@link KeyProvider} bean.
     *
     * @param publicKeyRepoBaseUrl base URL of the public key repository. It may be a composite URL.
     * @return a public key provider
     */
    @Bean
    public KeyProvider<PublicKey> publicKeyProvider(@Value("${asap.public_key_repository.url}") String publicKeyRepoBaseUrl) {
        return new PublicKeyProviderFactory(asapHttpClient(), new PemReader())
                .createPublicKeyProvider(getCombinedPublicKeyRepositoryBaseUrl(publicKeyRepoBaseUrl));
    }

    @VisibleForTesting
    String getCombinedPublicKeyRepositoryBaseUrl(String publicKeyRepoBaseUrl) {
        return StringUtils.isBlank(publicKeyRepositoryAdditionalUrl) ?
                publicKeyRepoBaseUrl : publicKeyRepoBaseUrl + " , " + publicKeyRepositoryAdditionalUrl;
    }

    /**
     * Definition of the {@link HttpClient} bean.
     *
     * @return a HTTP client
     */
    @Bean
    @Lazy
    @Qualifier("asap")
    public HttpClient asapHttpClient() {
        return HttpPublicKeyProvider.defaultHttpClient();
    }

    /**
     * Definition of the {@link RequestAuthenticator} bean.
     *
     * @param jwtValidator the validator of tokens
     * @return a request authenticator that authenticates valid tokens
     */
    @Bean
    public RequestAuthenticator requestAuthenticator(JwtValidator jwtValidator) {
        return new RequestAuthenticatorImpl(jwtValidator);
    }

    /**
     * @return the audience that the server accepts requests for
     */
    public String getAudience() {
        return audience;
    }
}
