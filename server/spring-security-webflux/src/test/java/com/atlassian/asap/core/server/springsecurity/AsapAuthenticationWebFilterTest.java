package com.atlassian.asap.core.server.springsecurity;

import org.junit.Test;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.server.authentication.ServerAuthenticationFailureHandler;
import org.springframework.security.web.server.authentication.ServerAuthenticationSuccessHandler;
import org.springframework.security.web.server.context.ServerSecurityContextRepository;
import org.springframework.security.web.server.util.matcher.ServerWebExchangeMatcher;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.function.Function;

import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;

public class AsapAuthenticationWebFilterTest {
    @Test
    public void itShouldCallParentSettersFromConstructor() {
        TestAsapAuthenticationWebFilter webFilter = new TestAsapAuthenticationWebFilter(mock(ReactiveAuthenticationManager.class));
        assertNotNull(webFilter);
    }

    /**
     * methods calls from constructor is a bad practice generally (methods can be overridden).
     * so, the solution could be to make a class final but it is not always acceptable for spring beans.
     * the simplest way is to just explicitly call {@code super.setAuthenticationConverter(authenticationConverter)}
     * and {@code super.setRequiresAuthenticationMatcher(requiresAuthenticationMatcher)} from constructor.
     */
    class TestAsapAuthenticationWebFilter extends AsapAuthenticationWebFilter {

        TestAsapAuthenticationWebFilter(ReactiveAuthenticationManager authenticationManager) {
            super(authenticationManager);
        }

        @Override
        public void setSecurityContextRepository(ServerSecurityContextRepository securityContextRepository) {
            throw new RuntimeException();
        }

        @Override
        public void setAuthenticationSuccessHandler(ServerAuthenticationSuccessHandler authenticationSuccessHandler) {
            throw new RuntimeException();
        }

        @Override
        public void setAuthenticationConverter(Function<ServerWebExchange, Mono<Authentication>> authenticationConverter) {
            throw new RuntimeException();
        }

        @Override
        public void setAuthenticationFailureHandler(ServerAuthenticationFailureHandler authenticationFailureHandler) {
            throw new RuntimeException();
        }

        @Override
        public void setRequiresAuthenticationMatcher(ServerWebExchangeMatcher requiresAuthenticationMatcher) {
            throw new RuntimeException();
        }
    }
}
