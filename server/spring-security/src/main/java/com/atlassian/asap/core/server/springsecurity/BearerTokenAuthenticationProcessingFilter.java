package com.atlassian.asap.core.server.springsecurity;

import com.atlassian.asap.core.JwtConstants;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.matcher.RequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Objects;

/**
 * Authentication processing filter that extracts the bearer token from the HTTP Authorization header. This filter
 * must be added to the chain, e.g., before
 * {@link org.springframework.security.web.authentication.www.BasicAuthenticationFilter}, and it works in coordination
 * with {@link AsapAuthenticationProvider}.
 */
public class BearerTokenAuthenticationProcessingFilter extends AbstractAuthenticationProcessingFilter {
    private static final AuthenticationSuccessHandler NOOP_SUCCESS_HANDLER =
            (httpServletRequest, httpServletResponse, authentication) -> { };
    private static final RequestMatcher REQUEST_MATCHER = new BearerTokenRequestMatcher();

    private final AuthenticationManager authenticationManager;

    @Autowired
    public BearerTokenAuthenticationProcessingFilter(AuthenticationManager authenticationManager) {
        super(REQUEST_MATCHER);
        this.authenticationManager = Objects.requireNonNull(authenticationManager);
        this.setAuthenticationSuccessHandler(NOOP_SUCCESS_HANDLER);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest httpServletRequest,
                                                HttpServletResponse httpServletResponse)
            throws AuthenticationException, IOException, ServletException {
        // due to the request matcher, it is guaranteed that the request contains a bearer token
        String authorizationHeader = httpServletRequest.getHeader(HttpHeaders.AUTHORIZATION);
        String serializedJwt = StringUtils.removeStart(authorizationHeader,
                JwtConstants.HTTP_AUTHORIZATION_HEADER_VALUE_PREFIX);
        Authentication candidateAuthentication = new UnverifiedBearerToken(serializedJwt);
        return authenticationManager.authenticate(candidateAuthentication);
    }

    // the implementation from the parent delegates to a successHandler, which unfortunately cannot
    // propagate the request down the filter chain, so we decorate the method
    @Override
    protected void successfulAuthentication(HttpServletRequest request,
                                            HttpServletResponse response,
                                            FilterChain chain,
                                            Authentication authResult)
            throws IOException, ServletException {
        super.successfulAuthentication(request, response, chain, authResult);
        chain.doFilter(request, response);
    }
}
