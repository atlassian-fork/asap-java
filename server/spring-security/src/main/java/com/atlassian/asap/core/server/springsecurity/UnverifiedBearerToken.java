package com.atlassian.asap.core.server.springsecurity;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Objects;

/**
 * An unparsed and unvalidated JWT token extracted from the Authorization header by {@link BearerTokenAuthenticationProcessingFilter}, and
 * accepted by {@link AsapAuthenticationProvider} for validation.
 */
class UnverifiedBearerToken implements Authentication {
    private static final long serialVersionUID = -2130699769215717838L;

    private static final String TOKEN_NAME = "Unverified JWT token";

    private final String bearerToken;

    UnverifiedBearerToken(String bearerToken) {
        this.bearerToken = Objects.requireNonNull(bearerToken);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return Collections.emptySet();
    }

    @Override
    public Object getCredentials() {
        return bearerToken; // the valid signature is the credential
    }

    @Override
    public Object getDetails() {
        return null;
    }

    @Override
    public Object getPrincipal() {
        return bearerToken;
    }

    @Override
    public boolean isAuthenticated() {
        return false;
    }

    @Override
    public void setAuthenticated(boolean isAuthenticated) throws IllegalArgumentException {
        if (isAuthenticated) {
            throw new IllegalArgumentException("This token is immutable");
        }
    }

    @Override
    public String getName() {
        return TOKEN_NAME;
    }
}
