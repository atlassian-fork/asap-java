package com.atlassian.asap.core.server.springsecurity;

import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.mock.web.MockHttpServletRequest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class BearerTokenRequestMatcherTest {
    private MockHttpServletRequest requestMock = new MockHttpServletRequest();

    @Test
    public void shouldRejectRequestWithoutAuthorizationHeader() {
        assertFalse(new BearerTokenRequestMatcher().matches(requestMock));
    }

    @Test
    public void shouldRejectRequestWithAuthorizationThatIsNotABearerToken() {
        requestMock.addHeader(HttpHeaders.AUTHORIZATION, "not-a-bearer-token");
        assertFalse(new BearerTokenRequestMatcher().matches(requestMock));
    }

    @Test
    public void shouldAcceptRequestWithBearerTokenInTheAuthorizationHeader() {
        requestMock.addHeader(HttpHeaders.AUTHORIZATION, "Bearer some-token");
        assertTrue(new BearerTokenRequestMatcher().matches(requestMock));
    }
}
