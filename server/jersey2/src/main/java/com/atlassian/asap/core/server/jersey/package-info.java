
/**
 * Jersey 2 adapter for ASAP
 *
 * <p> This adapter works a bit differently than other adapters by requiring the {@link
 * com.atlassian.asap.core.server.jersey.Asap} annotation to be placed on a resource package, class, or method level to
 * trigger the ASAP authentication and authorization.
 *
 * <h2>Installation</h2>
 *
 * <p> To install the authentication and authorization filters, you need to add a few singletons to
 * your Jersey configuration:
 * <pre>
 *  jerseyConfig.register(AuthenticationRequestFilter.newInstance(AUDIENCE, PUBLIC_KEY_URL));
 *  jerseyConfig.register(AuthorizationRequestFilter.newInstance());
 * </pre>
 * <p> These filters will create, validate, and authorize the jwt token for the request.
 *
 * <h2>Usage</h2>
 *
 * <p> To add ASAP
 * authenticate and authorization to your resource, just add the {@link com.atlassian.asap.core.server.jersey.Asap}
 * annotation to your resource package, class, or method.  This is an example of adding it on a class level:
 * <pre>
 *  &#64;Asap(authorizedSubjects = "mysubject")
 *  public class MyResource {
 *      void viewFoo() {}
 *  }
 * </pre>
 *
 * <p> To retrieve the {@link com.atlassian.asap.api.Jwt} token from the resource, get a
 * {@link javax.ws.rs.container.ContainerRequestContext} by injection. You can downcast the
 * {@link javax.ws.rs.core.SecurityContext} to an instance of {@link com.atlassian.asap.core.server.jersey.JwtSecurityContext}
 * and use it to retrieve the principal or the authenticated {@link com.atlassian.asap.api.Jwt} token.
 *
 * <h3>Environment variable configuration</h3>
 * To configure the subject and issuer whitelists via environment variables instead of hard-coding the values into
 * annotations, you can instead create the {@link com.atlassian.asap.core.server.jersey.AuthorizationRequestFilter}
 * instance like so:
 * <pre>
 *     new AuthorizationRequestFilter(new EmptyBodyFailureHandler(), AsapValidator.newEnvironmentVariablesValidator());
 * </pre>
 * The default environment variables are found on
 * {@link com.atlassian.asap.core.server.jersey.WhiteListAsapValidator.EnvironmentVariablesWhitelistProvider}.
 */
package com.atlassian.asap.core.server.jersey;